﻿using AutoMapper;
using Otus.Teaching.PromoCodeFactory.Core.Domain.Administration;
using Otus.Teaching.PromoCodeFactory.WebHost.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Otus.Teaching.PromoCodeFactory.WebHost.Helpers
{
    public class AutoMapperProfile : Profile
    {
        public AutoMapperProfile()
        {
            CreateMap<Employee, CreateOrUpdateEmployeeDto>();
            CreateMap<CreateOrUpdateEmployeeDto, Employee>().ForMember(e => e.FullName, o => o.MapFrom(src => src.FirstName + " " + src.LastName));
        }

    }
}
