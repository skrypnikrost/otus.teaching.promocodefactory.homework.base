﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using Microsoft.AspNetCore.Mvc;
using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories;
using Otus.Teaching.PromoCodeFactory.Core.Domain.Administration;
using Otus.Teaching.PromoCodeFactory.WebHost.Models;

namespace Otus.Teaching.PromoCodeFactory.WebHost.Controllers
{
    /// <summary>
    /// Сотрудники
    /// </summary>
    [ApiController]
    [Route("api/v1/[controller]")]
    public class EmployeesController
        : ControllerBase
    {
        private readonly IRepository<Employee> _employeeRepository;
        private readonly IMapper _mapper;

        public EmployeesController(IRepository<Employee> employeeRepository, IMapper mapper)
        {
            _employeeRepository = employeeRepository;
            _mapper = mapper;
        }

        /// <summary>
        /// Получить данные всех сотрудников
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public async Task<List<CreateOrUpdateEmployeeDto>> GetEmployeesAsync()
        {
            var employees = await _employeeRepository.GetAllAsync();

            return _mapper.Map<List<CreateOrUpdateEmployeeDto>>(employees);
        }

        /// <summary>
        /// Получить данные сотрудника по Id
        /// </summary>
        /// <returns></returns>
        [HttpGet("{id:guid}")]
        public async Task<ActionResult<CreateOrUpdateEmployeeDto>> GetEmployeeByIdAsync(Guid id)
        {
            var employee = await _employeeRepository.GetByIdAsync(id);

            if (employee == null)
                return NotFound();

            return _mapper.Map<CreateOrUpdateEmployeeDto>(employee);
        }

        /// <summary>
        /// Добавить новую сущность Employee
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        public async Task<ActionResult<EmployeeResponse>> AddEmployeeAsync(CreateOrUpdateEmployeeDto employeeDto)
        {
            var employee = _mapper.Map<Employee>(employeeDto);
            employee.Id = Guid.NewGuid();
            await _employeeRepository.CreateAsync(employee);

            return Ok();
        }

        /// <summary>
        /// Обновить сущность Employee
        /// </summary>
        /// <returns></returns>
        [HttpPut("{id}")]
        public async Task<ActionResult<CreateOrUpdateEmployeeDto>> UpdateEmployeeAsync(Guid id, [FromBody] CreateOrUpdateEmployeeDto entity)
        {
            var currentEmployee = await _employeeRepository.GetByIdAsync(id);

            if (currentEmployee == null)
                return NotFound();

            if (entity.Roles.Count == 0)
            {
                currentEmployee.Roles.Clear();
            }
            var updateEmployee = _mapper.Map(entity, currentEmployee);
            await _employeeRepository.UpdateAsync(updateEmployee);

            return _mapper.Map<CreateOrUpdateEmployeeDto>(updateEmployee); ;
        }

        /// <summary>
        /// Удалить сущность Employee
        /// </summary>
        /// <returns></returns>
        [HttpDelete("{id:guid}")]
        public async Task<ActionResult> RemoveEmployeeAsync(Guid id)
        {
            var entity = _employeeRepository.GetByIdAsync(id);
            if (entity != null)
            {
                await _employeeRepository.RemoveAsync(id);
            }
            else
            {
                return BadRequest("Entity for delete doesn't exsist");
            }

            return Ok();
        }
    }
}